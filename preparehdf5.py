import h5py, os
import caffe
import numpy as np
import glob


SIZE = 384 # fixed size to all images
img_files = glob.glob('data/ADEChallengeData2016/images/training/*.jpg')
#N = len(img_files)
N = 1
# If you do not have enough memory split data into
# multiple batches and generate multiple separate h5 files
X = np.zeros( (N, 3, SIZE, SIZE), dtype='f4' ) 
y = np.zeros( (N,1,SIZE,SIZE), dtype='f4' )
#for file in img_files:
for i in range(N):
    file =img_files[i]
    predFile = file
    predFile.replace('images','annotations')
    predFile.replace('ADE_train_','ADE_val_')
    img = caffe.io.load_image( file)
    img = caffe.io.resize( img, (3,SIZE, SIZE) ) # resize to fixed size
    imgPred = caffe.io.load_image(predFile)
    imgPred = caffe.io.resize( img, (1,SIZE, SIZE) )
    # you may apply other input transformations here...
    X[i] = img
    y[i] = imgPred
with h5py.File('train.h5','w') as H:
    H.create_dataset( 'X', data=X ) # note the name X given to the dataset!
    H.create_dataset( 'y', data=y ) # note the name y given to the dataset!
with open('train_h5_list.txt','w') as L:
    L.write( 'train.h5' ) # list all h5 files you are going to use

import numpy as np
import cv2
import random


def ConvertToOneHotEncoding(label,NumOfClasses) :
	l = len(label)
	newLabel = np.zeros([l,NumOfClasses])
	for i in range(l) :
		newLabel[i][int(label[i])] = 1

	return newLabel

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  '''
  fan_in = shape[0]
  fan_out = shape[1]
  low = -4*np.sqrt(6.0/(fan_in + fan_out)) # use 4 for sigmoid, 1 for tanh activation 
  high = 4*np.sqrt(6.0/(fan_in + fan_out))
  return tf.Variable(tf.random_uniform(shape, minval=low, maxval=high, dtype=tf.float32))
  '''
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.01, shape=shape)
  return tf.Variable(initial)

def getBatchInputData(inp,label,batch_size,start) :
  first = start
  start = start + batch_size
  length = len(inp)
  x1 = length
  #labels = np.zeros([batch_size,1])
  if start > x1 :
    first = 0
    perm = np.arange(x1)
    np.random.shuffle(perm)
    inp = inp[perm]
    label = label[perm]
    start = batch_size
  end = start
  #labels[:,0] = label[first:end]
  return inp[first:end] , label[first:end] , start , inp, label

def GetData(imageLocation,annotationLocation,training = True) :
	
	if training :
		data = np.zeros([2500*20000,27])
		label = np.zeros([2500*20000])
		files = 20000
	else :
		data = np.zeros([2500*2000,27])
		label = np.zeros([2500*2000])
		files = 2000
	count = 0
	for h in range(files) :
		if h%100 == 0 :
			print('%d steps reached'%h)
		filename = str('00000000'+ str(h+1))
		filename = filename[len(filename)-8:]
		#print filename
		img = cv2.imread(imageLocation+filename+'.jpg')
		annotation = cv2.imread(annotationLocation+filename+'.png')
		x,y,z = img.shape
		#x1,y1,z1 = annotation.shape
		a = np.zeros([x+2,y+2,3])

		#print img.shape
		a[1:x+1,1:y+1,:] = img

		arrx = random.sample(range(1,x+1),50)
		arry = random.sample(range(1,y+1),50)

		#print arrx
		#print arry

		
		for i in range(50) :
			x1 = arrx[i]
			for j in range(50) :
				y1 = arry[j]	
				data[count][0:3] = a[x1-1,y1-1:y1+2,0]
				data[count][3:6] = a[x1-1,y1-1:y1+2,1]
				data[count][6:9] = a[x1-1,y1-1:y1+2,2]
				data[count][9:12] = a[x1,y1-1:y1+2,0]
				data[count][12:15] = a[x1,y1-1:y1+2,1]
				data[count][15:18] = a[x1,y1-1:y1+2,2]
				data[count][18:21] = a[x1+1,y1-1:y1+2,0]
				data[count][21:24] = a[x1+1,y1-1:y1+2,1]
				data[count][24:27] = a[x1+1,y1-1:y1+2,2]
				label[count] = annotation[x1-1,y1-1,0]
				count = count+1

	print 'hi'
	return data,label
#np.savetxt('trainingfile.csv',Validationdata,delimiter = ",")
		
imageLocation = '/home/abhishek/Downloads/ADEChallengeData2016/images/validation/ADE_val_'
annotationLocation = '/home/abhishek/Downloads/ADEChallengeData2016/annotations/validation/ADE_val_'
validationData,validationLabel = GetData(imageLocation,annotationLocation,False)
print 'Validation data obtained'

imageLocation = '/home/abhishek/Downloads/ADEChallengeData2016/images/training/ADE_train_'
annotationLocation = '/home/abhishek/Downloads/ADEChallengeData2016/annotations/training/ADE_train_'
trainingData,trainingLabel = GetData(imageLocation,annotationLocation)

print 'Training data obtained'

NumOfClasses = 151
trainLabelOneHot = ConvertToOneHotEncoding(trainingLabel,NumOfClasses)
validLabelOneHot = ConvertToOneHotEncoding(validationLabel,NumOfClasses)

training_iters = 100000
learning_rate = 0.02
batch_size = 256
lr_decay_rate = 0.75
lr_decay_step = 25000
CheckpointStep = 10000
displayStep = 1000

x = tf.placeholder(tf.float32, shape=[None, 27])
y_ = tf.placeholder(tf.float32, shape=[None, 151])

W_fc1 = weight_variable([27,100])
b_fc1 = bias_variable([100])

h1 = tf.nn.relu(tf.matmul(x,W_fc1)+b_fc1)


W_fc2 = weight_variable([100,200])
b_fc2 = bias_variable([200])


h2 = tf.nn.relu(tf.matmul(h1,W_fc2) + b_fc2)

keep_prob = tf.placeholder(tf.float32)
h2_drop = tf.nn.dropout(h2, keep_prob)



W_fc3 = weight_variable([200,100])
b_fc3 = bias_variable([100])


h3 = tf.nn.relu(tf.matmul(h2_drop,W_fc3) + b_fc3)

W_output = weight_variable([100,151])
b_output = bias_variable([151])

y = tf.nn.softmax(tf.matmul(h3,W_output) + b_output)


cross_entropy = -tf.reduce_sum(y_*tf.log(y))


global_step = tf.Variable(0, trainable=False)
lr = tf.train.exponential_decay(learning_rate,
								global_step,
								lr_decay_step,
								lr_decay_rate,
								staircase=True)
								
train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

sess = tf.Session()
saver = tf.train.Saver()

init = tf.initialize_all_variables()
sess.run(init)

start = 0

for i in range(training_iters) :  

  inp,out,start,trainingData,trainLabelOneHot = getBatchInputData(trainingData,trainLabelOneHot,batch_size,start)

  
  if i %displayStep == 0 :
  	
  	print('%d steps reached'%i)
  	correct = sess.run([accuracy],feed_dict={x: validationData, y_: validLabelOneHot, keep_prob : 1.0})
  	
  	print('after %d steps the accuracy is %g'%(i,correct))

    
  if i % CheckpointStep == 0 or i == training_iters - 1:
    save_path = saver.save(sess, savingModel)
    print("Model saved in file: %s" % save_path)  
  

  sess.run([train_step],feed_dict={x: inp, y_: out, keep_prob : 0.5})
    
print 'training done' 


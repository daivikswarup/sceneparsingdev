import os
import caffe
from PIL import Image
import numpy as np
import pickle
import random
## TO extract and save intermediate conv outputs 
model_definition = 'models/truncmode.prototxt';
model_weights = 'FCN_iter_160000.caffemodel';

net = caffe.Net(model_definition, model_weights, caffe.TEST);

transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
transformer.set_mean('data', np.load('/home/daivik/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1))
transformer.set_transpose('data', (2,0,1))
transformer.set_channel_swap('data', (2,1,0)) 

dat_x = []
dat_y = []
cnt = 0
for f in os.listdir('/home/daivik/sceneparsing/ADEChallengeData2016/images/training'):
	img = Image.open('/home/daivik/sceneparsing/ADEChallengeData2016/images/training/'+f)
	img = img.resize((384,384))
	net.blobs['data'].data[...] = transformer.preprocess('data', np.array(img))
	out = net.forward()
	info = out['conv1_2']
	info = info[0,:,100:484,100:484]#correct for convolutions
	annotation = Image.open('/home/daivik/sceneparsing/ADEChallengeData2016/annotations/training/'+f.replace('.jpg','.png'))
	annotation = np.array(annotation.resize( (384,384) ) )
	arrx = random.sample(range(0,383),5)
	arry = random.sample(range(0,383),5)
	#get only 25 for memory's sake
	for i in range(5):
		for j in range(5):
			dat_x.append(info[:,i,j])
			dat_y.append(annotation[i,j])
	cnt = cnt + 1
	print(cnt)
dick = {'x':dat_x, 'y':dat_y}
f=open('dat_for_svm.pkl','w')
pickle.dump(dick,f)



import numpy as np
import cv2
import random
import math



imageLocation = '/home/daivik/sceneparsing/ADEChallengeData2016/images/training/ADE_train_'
annotationLocation = '/home/daivik/sceneparsing/ADEChallengeData2016/annotations/training/ADE_train_'
numOfClasses = 151
count = np.zeros([32,32,32,numOfClasses])
files = 20000

for h in range(files) :
	if h%10 == 0 :
		print('%d steps reached'%h)
	filename = str('00000000'+ str(h+1))
	filename = filename[len(filename)-8:]
	#print filename
	img = cv2.imread(imageLocation+filename+'.jpg')
	annotation = cv2.imread(annotationLocation+filename+'.png')
	x,y,z = img.shape
	for i in range(x) :
		for j in range(y) :
			label = annotation[i][j][0]
			red = math.floor(img[i][j][0]/8)
			green = math.floor(img[i][j][1]/8)
			blue = math.floor(img[i][j][2]/8)
			count[red][green][blue][label]+=1

valImageLoc = '/home/daivik/sceneparsing/ADEChallengeData2016/images/validation/ADE_val_'
valAnnotationLoc = '/home/daivik/sceneparsing/ADEChallengeData2016/annotations/validation/ADE_val_'	
testSize = 100
prob = np.zeros([numOfClasses])

for h in range(testSize) :
	if h%10 == 0 :
		print('%d steps reached'%h)
	filename = str('00000000'+ str(h+1))
	filename = filename[len(filename)-8:]
	img = cv2.imread(valImageLoc+filename+'.jpg')
	x,y,z = img.shape
	for i in range(x) :
		for j in range(y) :
			total = 0
			red = math.floor(img[i][j][0]/8)
			green = math.floor(img[i][j][1]/8)
			blue = math.floor(img[i][j][2]/8)
			for k in range(numOfClasses) :
				total+=count[red][green][blue][k]
			prob = count[red,blue,green,:]
			cummProb = [sum(prob[:i + 1]) / total
						   for i in xrange(numOfClasses)]
			random_number_01 = np.random.random_sample()
			pos = min([i for i in xrange(numOfClasses)
					   if cummProb[i] > random_number_01])
			img[i][j][0] = pos
			img[i][j][1] = pos
			img[i][j][2] = pos
	cv2.imshow('image',img)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
		

					






	#cv2.imshow('image',img)
	#cv2.waitKey(0)
	#cv2.destroyAllWindows()


